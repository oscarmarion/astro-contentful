import type { FunctionalComponent } from "preact";
import { useState } from "preact/hooks";
import menu from "../../assets/icons/menu.svg"; // svgReference === '/src/image.svg'
import cross from "../../assets/icons/cross.svg"; // svgReference === '/src/image.svg'

interface MenuItemType {
    title: string;
    slug: string;
}

export const MobileNavigation: FunctionalComponent<{ items: Array<MenuItemType> }> = ({ items }) => {
    const [isVisible, setIsVisible] = useState<boolean>(false);

    const handleMenuVisibility = (value: boolean) => {
        setIsVisible(value);
        if (value) {
            document.body.classList?.add("overflow-hidden");
            return;
        }
        document.body.classList?.remove("overflow-hidden");
    };
    return (
        <>
            <button onClick={() => handleMenuVisibility(!isVisible)} className="text-black block">
                {isVisible ? (
                    <img src={cross} height="32" width="32" className="block" />
                ) : (
                    <img src={menu} height="32" width="32" className="block" />
                )}
            </button>
            {!!isVisible && (
                <ul className="absolute top-16 left-0 w-full h-full flex flex-col gap-8 py-8 backdrop-blur backdrop-brightness-110">
                    {items.map(({ slug, title }) => (
                        <li className="font-bold text-orange-800 text-center text-xl">
                            <a href={`/${slug === "homepage" ? "" : slug}`}>{title}</a>
                        </li>
                    ))}
                </ul>
            )}
        </>
    );
};
